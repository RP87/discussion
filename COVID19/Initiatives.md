# Initiatives to fight COVID-19

## Sites indexing and listing worldwide initiatives

### [CivicTechHub](https://civictechhub.netlify.com/)

CivicTechHub indexes and lists initiatives from more than 29 countries to create visibility of social projects and activities like 
Covid-19 crisis-inspired hackathon [WirVsVirus](https://wirvsvirushackathon.org/), government initiatives and local exchange groups.

- [Mega-list of initiatives](https://docs.google.com/document/d/e/2PACX-1vS2p8BPJ5d0WcHxRAB0BWCv6fY2lgZPVanfZNqOX4z2e00cOEwPIWhlO5ZNlPb5Fe2Pva_c74leKrR2/pub?urp=gmail_link)


## Resource sharing initiatives

### [Global MediXchange for Combating COVID-19 (GMCC)](https://covid-19.alibabacloud.com)

> The Jack Ma Foundation and Alibaba Foundation jointly established the Global MediXchange for Combating COVID-19 (GMCC) programme, 
with the support of Alibaba Cloud Intelligence and Alibaba Health, to help combat the global outbreak of the novel coronavirus, COVID-19. 
This programme was established to facilitate online communication and collaboration across borders, 
as well as provide frontline medical teams around the world with the necessary communication channels 
to share practical experience about fighting the pandemic.

Public resources:

- [Handbook of COVID-19 Prevention and Treatment](https://gitlab.com/publishing-reform/discussion/-/blob/master/COVID19/Public.md#handbook-of-covid-19-prevention-and-treatment)



## Collaborative challenges

### [COVID-19 Open Research Dataset Challenge (CORD-19)](https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge)

An AI challenge with AI2, CZI, MSR, Georgetown, NIH & The White House

- [Call to Action to the Tech Community on New Machine Readable COVID-19 Dataset](https://www.whitehouse.gov/briefings-statements/call-action-tech-community-new-machine-readable-covid-19-dataset/)
- [Call for collaboration by Artur Kiulian on LinkedIn](https://www.linkedin.com/feed/update/urn:li:activity:6646040949627715584/)



## Collaboration initiatives

### [Just One Giant Lab (JOGL)](https://jogl.io/)

The first research and innovation laboratory operating as a distributed, open and massive mobilisation platform for collaborative task solving.

[Guardian - Biohackers team up online to help develop coronavirus solutions](https://www.theguardian.com/world/2020/mar/18/biohacking-online-forums-coronavirus-vaccines-testing)

#### JOGL Projects

- [Low-cost & Open-Source Covid19 Detection kits](https://app.jogl.io/project/118)
- [Contact tracing COVID19](https://app.jogl.io/project/137)



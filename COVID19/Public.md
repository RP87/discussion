# Public information sources

All contributions are welcome!
Please mention sources when posting to ensure maximum credibility.

## [Coronavirus Tech Handbook](https://docs.google.com/document/d/111k1L5D9TZNShV5Gr2AJ7grfuXmEx_dYXAZcNwEebQI/edit#heading=h.abb7jnjb11f1)
edited by Ruth Ann Crystal MD [@CatchTheBaby](https://twitter.com/catchthebaby) https://catchthebaby.com/
- [Twitter announcement](https://twitter.com/CatchTheBaby/status/1238420300037005312)

## [Handbook of COVID-19 Prevention and Treatment](https://covid-19.alibabacloud.com/) 
By the First Affiliated Hospital, Zhejiang University School of Medicine, part of the Global MediXchange for Combating COVID-19 (GMCC) 
programme by The Jack Ma Foundation and Alibaba Foundation


> The First Affiliated Hospital, Zhejiang University School of Medicine has treated 104 patients tested positive for COVID-19 in 50 days; and it has achieved zero deaths in patients diagnosed, zero patients misdiagnosed, and zero infections in medical staff. Their experts wrote real treatment experience when combating the virus night and day, and quickly published this Handbook of COVID-19 Prevention and Treatment, in order to share their invaluable practical advice and references with medical professionals around the world. This handbook compares and analyzes the experience of other experts in China; gives detailed and comprehensive answers to potential questions for diagnosing and treating patients in different types; and provides a good reference to key departments such as hospital infection management, nursing, and outpatient clinics. This handbook provides comprehensive guidelines and best practices by China's top experts for coping with COVID-19.

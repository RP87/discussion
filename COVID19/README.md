# Community resources - everyone is welcome to contribute!

This is the top level directory for resources and projects worldwide that are COVID19 related in a broad sense,
with particular emphasis on the academic research and peer-reviewed sources.

Please feel welcome to share your ideas, information or link to any other project you know.


## Funtionality available on Gitlab

1. Text or files in any format can be shared within this repository. 
1. PDF files can be uploaded and viewed within Gitlab.
1. Entire repository conent can be downloaded and kept updated with [Git](https://git-scm.com/).
1. Members with maintainers rights can add/edit/delete any content. 
1. Anyone can submit a request to add/edit/delete content that can be approved by any maintainer.
1. As all versions and changes are recorded, no content gets lost.
1. All files and sections can be linked from outside. E.g. here is [the link to this section](https://gitlab.com/publishing-reform/discussion/-/tree/master/COVID19#available-funcionality-highlights). File/section names become part of the URL, so please use keep them short.


See here more information about this forum and available functionality:
https://gitlab.com/publishing-reform/discussion

## Content changes

If you want to change directory/file/section names or move files to other directories, 
here are suggestions to ensure links from outside don't break:

1. Keep the old directory/file/section. 
1. Add your new directory/file/section beside the old one.
1. Update the content and move relevant parts to the new directory/file/section.
1. Add the change information about links to the new content at the top of the old file.

That way, anyone following the old link will see the update information at the top of the file and can follow the links to see the updates.


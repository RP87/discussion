# Research on COVID19 Monitoring - selected articles

Ordered by date of publication, more recent at the top.

All contributions are welcome!

1. Please mark any article that is NOT peer-reviewed.
1. For any article without open access please also link to its free version if possilbe (use https://unpaywall.org/ to find the free versions).


#### Mar 16, 2020: Kwon KT, Ko JH, Shin H, Sung M, Kim JY.   Drive-Through Screening Center for COVID-19: a Safe and Efficient Screening System against Massive Community Outbreak. J Korean Med Sci. 2020 Mar;35(11):e123.   https://doi.org/10.3346/jkms.2020.35.e123

Abstract.
As the coronavirus disease 2019 (COVID-19) outbreak is ongoing, the number of individuals to be tested for COVID-19 is rapidly increasing. For safe and efficient screening for COVID-19, drive-through (DT) screening centers have been designed and implemented in Korea. Herein, we present the overall concept, advantages, and limitations of the COVID-19 DT screening centers. The steps of the DT centers include registration, examination, specimen collection, and instructions. The entire service takes about 10 minutes for one testee without leaving his or her cars. Increased testing capacity over 100 tests per day and prevention of cross-infection between testees in the waiting space are the major advantages, while protection of staff from the outdoor atmosphere is challenging. It could be implemented in other countries to cope with the global COVID-19 outbreak and transformed according to their own situations.


#### Mar 3, 2020: Wang CJ, Ng CY, Brook RH. Response to COVID-19 in Taiwan: Big Data Analytics, New Technology, and Proactive Testing. JAMA. Published online March 03, 2020. https://doi.org/10.1001/jama.2020.3151


#### Feb 20, 2020: Wong JEL, Leo YS, Tan CC. COVID-19 in Singapore—Current Experience: Critical Global Issues That Require Attention and Action. JAMA. Published online February 20, 2020. https://doi.org/10.1001/jama.2020.2467


####  Feb 03, 2020: Kim JY, Choe PG, Oh Y, Oh KJ, Kim J, Park SJ, Park JH, Na HK, Oh MD.   The First Case of 2019 Novel Coronavirus Pneumonia Imported into Korea from Wuhan, China: Implication for Infection Prevention and Control Measures.   J Korean Med Sci. 2020 Feb;35(5):e61.   https://doi.org/10.3346/jkms.2020.35.e61

Abstract.
In December 2019, a viral pneumonia outbreak caused by a novel betacoronavirus, the 2019 novel coronavirus (2019-nCoV), began in Wuhan, China. We report the epidemiological and clinical features of the first patient with 2019-nCoV pneumonia imported into Korea from Wuhan. This report suggests that in the early phase of 2019-nCoV pneumonia, chest radiography would miss patients with pneumonia and highlights taking travel history is of paramount importance for early detection and isolation of 2019-nCoV cases.

#### Feb 03, 2020: Yoo JH, Hong ST.   The Outbreak Cases with the Novel Coronavirus Suggest Upgraded Quarantine and Isolation in Korea.   J Korean Med Sci. 2020 Feb 03;35(5):e62.   https://doi.org/10.3346/jkms.2020.35.e62


Details of inexpensive publishing service providers
======

## [Scholastica (click for more information)](https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/Scholastica%20-%20A%20modern%20journal%20management%20system.md)


## [Centre Mersenne (click for more information)] (https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/Mersenne.md)


## [T&T (click for more information)] (https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/T&T.md)


## [Ubiquity Press (click for more information)] (https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/Ubiquity.md) 


## [PsychOpen (click for more information)] (https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/PsychOpen.md)


## [Episciences (click for more information)](https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/Episciences.md)

## [PKP Publishing Services] (https://pkpservices.sfu.ca/) (creators of Open Journal Systems technical support and hosting service)
The signatories listed below all publicly support the [Fair Open Access Principles](https://www.fairopenaccess.org/). The list is not exhaustive and has been created by direct contact with "prominent" researchers
rather than public advertising. If you wish to sign, please contact Mark C. Wilson, wilson.mark.c@gmail.com.

The principles are:

1. The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.
2. Authors of articles in the journal retain copyright.
3. All articles are published open access and an explicit open access licence is used.
4. Submission and publication is not conditional in any way on the payment of a fee from the author or its employing institution, or on membership of an institution or society.
5. Any fees paid on behalf of the journal to publishers are low, transparent, and in proportion to the work carried out.

French version / version Française:

1. le journal a un ou des propriétaires clairement identifiés, il est contrôlé par la communauté académique et lui rend compte ;
2. les auteurs gardent la propriété intellectuelle complète des articles ;
3. tous les articles sont publiés en accès libre garanti par une licence explicite ;
4. la soumission et la publication ne sont conditionnées à aucun paiement par les auteurs ni par l'institution les employant, non plus qu'à leur appartenance à une société savante ou à une institution ;
5. tous les frais payés à des fournisseurs de service pour la publication du journal sont modérés, transparents, et en proportion du travail fourni.

Individual signatories
===

Mathematics/Statistics
----------------

* Adem, Alejandro - University of British Columbia
* Agol, Ian - University of California Berkeley
* Alon, Noga - Princeton University and Tel Aviv University
* Arnold, Douglas - University of Minnesota
* Atiyah, Michael - University of Edinburgh
* Baez, John - University of California Riverside
* Ball, John - University of Oxford
* Barcelo, Hélène - Mathematical Sciences Research Institute
* Bass, Hyman - University of Michigan
* Beck, Matthias - San Francisco State University
* Borcherds, Richard - University of California Berkeley
* Brent, Richard - Australian National University
* Bressoud, David - Macalester College
* Bugeaud, Yann - Université de Strasbourg
* Calegari, Danny - University of Chicago
* Chambert-Loir, Antoine - Université Paris Diderot
* Chafaï,  Djalil -  Université Paris-Dauphine
* Ciliberto, Ciro - University of Rome Tor Vergata
* Cohn, Henry -  Microsoft Research
* Demailly, Jean-Pierre - Université Grenoble Alpes
* Diaconis, Persi - Stanford University
* den Hollander, Frank - Universiteit Leiden
* Edixhoven, Bas - Universiteit Leiden
* Ellenberg, Jordan - University of Wisconsin
* Emerton, Matthew - University of Chicago
* Evans, Lawrence C. - University of California Berkeley
* Farge, Marie - École Normale Supérieure and CNRS
* Gee, Toby -  Imperial College London
* Gowers, Timothy - University of Cambridge
* Grötschel, Martin - Berlin-Brandenburgische Akademie der Wissenschaften
* Hairer, Martin - Imperial College London
* Hales, Thomas -  University of Pittsburgh
* Harris, Michael - Columbia University and Université Paris-Diderot
* Hélein, Frédéric - Université Paris-Diderot
* Hersh, Reuben - University of New Mexico
* Holmes, Susan - Stanford University
* Iserles, Arieh - University of Cambridge
* Iwaniec, Tadeusz - Syracuse University
* Joshi, Nalini - University of Sydney
* Joyal, André - Université du Québec à Montréal
* Karoubi, Max - Université Paris Diderot
* Kohn, Joseph J. - Princeton University
* Kuperberg, Krystyna - Auburn University
* Lafforgue, Vincent - Université de Grenoble and CNRS
* Lafforgue, Laurent - Institut des Hautes Études Scientifiques
* Laurent, Monique - CWI Amsterdam
* Lawler, Gregory - University of Chicago
* May, J. Peter - University of Chicago
* Meyer, Yves - École Normale Supérieure Paris-Saclay
* Morrison, Scott - Australian National University
* Mouhot, Clément  - University of Cambridge
* Mumford, David - Brown University
* Odlyzko, Andrew - University of Minnesota
* Olver, Peter - University of Minnesota
* Palais, Richard - University of California Irvine
* Pardon, John - Princeton University
* Reiner, Victor - University of Minnesota
* Solovay, Robert - University of California Berkeley
* Stein, Elias - Princeton University
* Taylor, Richard - Institute for Advanced Study
* van der Vaart, Aad - Universiteit Leiden
* Voloch, Felipe - University of Canterbury
* Wolf, Julia - University of Bristol
* Wooley, Trevor - University of Bristol
* Zeilberger, Doron - Rutgers University
* Zeitouni, Ofer - Weizmann Institute of Science
* Ziegler, Günter - Free University of Berlin 

Computer Science
-------------
* Aaronson, Scott - University of Texas Austin
* Apt, Krzysztof - CWI Amsterdam
* Baeten, Jos - CWI Amsterdam
* Barak, Boaz - Harvard University
* Chazelle, Bernard - Princeton University
* Eppstein, David - University of California Irvine
* Erickson, Jeff - University of Illinois
* Halpern, Joe - Cornell University
* LeCun, Yann - New York University and Facebook
* Menczer, Filippo - Indiana University
* Norvig, Peter - Google
* Salvy, Bruno - INRIA and ENS Lyon
* Walsh, Toby - University of New South Wales

Linguistics
-------------
* Abner, Natasha - University of Michigan
* Adelaar, Willem - Leiden University
* Adger, David - University of London
* Ahn, Byron - Princeton University
* Anderson, Stephen - Yale University
* Andrews, Avery - Australian National University
* Arabyan, Marc - University of Limoges
* Auer, Anita - University of Lausanne
* Bakovic, Eric - UCSD
* Bastiaanse, Roelien - University of Groningen
* Benincà, Paola - University of Padova
* Bertinetto, Pier Marco - Scuola Normale Superiore
* Bjorkman, Bronwyn - Queen's University
* Boersma, Paul - University of Amsterdam
* Booij, Geert - University of Leiden
* Carlin, Eithne - Leiden University
* Choi, Jiyoung - Institut National des Langues et Civilisations Orientales
* Chomsky, Noam - MIT and University of Arizona
* Chung, Karen Steffen - National Taiwan University
* Cinque, Guglielmo - Università Ca' Foscari Venezia
* Copley, Bridget - Centre Nationale de la Recherche Scientifique
* Costa, João  - Universidade Nova and Portugese Government 
* Crasborn, Onno - Radboud University
* Crevels, Mily - University of Leiden
* d’Alessandro, Roberta - Utrecht University
* de Vos, Mark - Rhodes University
* Demirdache, Hamida - University of Nantes
* Dingemanse, Marc - MPI for Psycholinguistics
* Doetjes, Jenny - University of Leiden
* Donati, Caterina - Universite Paris Diderot
* Eckardt, Regine - Universitat Konstanz
* Evans, Vyv - Independent Researcher
* Essegbey, James - University of Florida
* Fábregas, Antonio - University of Tromsø
* Faller, Martina - University of Manchester
* Fradin, Bernard - Paris Diderot University
* Francez, Itamar - University of Chicago
* Friedmann, Naama - Tel Aviv University
* Frota, Sonia - Universidade de Lisboa
* Froud, Karen - Columbia University
* Gehrke, Berit - Universite Paris Diderot
* Georgi, Doreen - Universität Potsdam
* Grohmann, Kleanthes - University of Cyprus
* Grosu, Alexander - Tel Aviv University
* Hall-Lew, Lauren - University of Edinburgh
* Han, Chung-hye - Simon Fraser University
* Harley, Heidi - University of Arizona
* Haspelmath, Martin - MPI for the Science of Human History
* Heycock, Caroline - University of Edinburgh
* Horesh, Uri - University of Essex
* Horvath, Julia - Tel Aviv University
* Jacques, Guillaume - Centre Nationale de la Recherche Scientifique
* Janda, Laura - Arctic University of Norway
* Jansen, Henrike - Leiden University
* Jouitteau, Mélanie - Centre Nationale de la Recherche Scientifique
* Kardos, Eva - University of Debrecen
* Kasstan, Jonathan - University of London
* Katz, Jonah - West Virginia University
* Kenesei, Istvan - University of Szeged
* Klamer, Marian - Leiden University
* Köhnlein, Björn - Ohio State University
* Kornfilt, Jaklin - Syracuse University
* Krämer, Martin - University of Tromsø
* Laca, Brenda - Universite Paris 8
* Lahrouchi, Mohamed - Centre Nationale de la Recherche Scientifique
* Lassiter, Daniel - Stanford University
* Leben, Will - Stanford University
* Lee, Thomas Hun-tak - Chinese University of Hong Kong
* Lubotzky, Alexander - Hebrew University of Jerusalem and Yale University
* Margulis, Daniel - MIT
* Matushansky, Ora - Centre Nationale de la Recherche Scientifique
* McCloskey, James - UC Santa Cruz
* McFadden, Thomas - Univerity of Pennsylvania
* McIntyre, Andrew - Humboldt-Universitat zu Berlin
* McNally, Louise - Universitat Pompeu Fabra
* Merchant, Jason - University of Chicago
* Mey, Jacob - University of Southern Denmark
* Mosegaard Hansen, Maj-Britt - University of Manchester
* Müller, Gereon - University of Leipzig
* Müller, Stefan - Humboldt-Universitat zu Berlin
* Muskens, Reinhard - Tilburg University
* Nash, Léa - Universite de Vincennes
* Neidle, Carol - Boston University
* Newell, Heather - McGill University
* Oiry, Magda - University of Massachusetts Amherst
* Ott, Dennis - University of Ottawa
* Oyharçabal, Bernard - Centre Nationale de la Recherche Scientifique
* Paul, Ileana - University of Western Ontario
* Paul, Waltraud - Centre Nationale de la Recherche Scientifique
* Pesetsky, David - MIT
* Peyraube, Alain - Centre Nationale de la Recherche Scientifique
* Pica, Pierre - Universidade Federal do Rio Grande do Norte
* Poletto, Cecilia - Goethe-Universitat Frankfurt am Main
* Pullum, Geoffrey - University of Edinburgh
* Puskas, Genoveva - University of Geneva
* Reboul, Anne - University of Lyon
* Richter, Frank - Goethe Universitat
* Rooryck, Johan - Leiden University
* Roussou, Anna - University of Patras
* Rouveret, Alain - Paris Diderot University
* Roy, Isabelle - Universite Paris 8
* Salzmann, Martin - University of Leipzig
* Schembri, Adam - University of Birmingham
* Schlenker, Philippe - Institut Jean Nicod
* Schwarz, Florian - University of Pennsylvania
* Sharvit, Yael - UCLA
* Szendroi, Kriszta - UCL
* Spector, Benjamin - Institut Jean Nicod
* Spencer, Andrew - University of Essex
* Sperber, Dan - Central European University
* Thieberger, Nick - University of Melbourne
* Tonhauser, Judith - Ohio State University
* Tessier, Anne-Michelle, University of Michigan
* Tsimpli, Ianthi - Aristotle University of Thessaloniki
* Vanden Wyngaerd, Guido - KU Leuven
* van Heuven, Vincent - Leiden University
* van Oostendorp, Marc - Radboud University
* van Reenen, Pieter - Free University of Amsterdam
* Vincent, Nigel - University of Manchester
* von Fintel, Kai - MIT
* von Heusinger, Klaus - Universitat zu Koln
* Walkden, George - University of Konstanz
* Wauquier, Sophie - Universite Paris 8
* Wellwood, Alexis - University of Southern California
* Zeijlstra, Hedde - Georg-August-University
* Zukoff, Sam - MIT
* Zwart, Jan-Wouter - University of Groningen

Psychology / Neuroscience
--------------
* Ansari, Daniel - Western Ontario
* Behrmann, Marlene - CMU
* Brady, Tim -	UC San Diego
* Carey, Susan - Harvard
* Csibra, Gergely - Central European University
* Creel, Sarah - UCSD
* Dehaene, Stanislas - Collège de France / INSERM
* Dunham, Yarrow -	Yale University
* Gallistel, Randy -	Rutgers University
* Gibson, Ted - MIT
* Goldberg, Adele -	Princeton University
* Goodman Noah - Stanford University
* Gopnik, Alison -	UC Berkeley
* Gweon, Hyowon	- Stanford University
* Kemp, Charles -	Carnegie Mellon University
* Lombrozo, Tania - UC Berkeley
* Marcus, Gary -	NYU
* Kanwisher, Nancy - MIT
* Legare, Cristine - UT Austin
* Lombrozo, Tania -	UC Berkeley
* Mahon, Bradford -	University of Rochester
* McDermott, Josh - MIT
* Pashler, Hal - University of California San Diego
* Perfors, Amy - University of Melbourne
* Piantadosi, Steve -	University of Rochester
* Reder, Lynn - CMU
* Saffran, Jenny -	University of Wisconsin-Madison
* Spelke, Elizabeth	-	Harvard University
* Sperber, Dan - Central European University and Institut Nicod
* Santos, Laurie -	Yale University
* Saxe, Rebecca -	MIT
* Srinivasan, Mahesh - UC Berkeley
* Vul, Ed - UCSD
* Wagenmakers, Eric-Jan - University of Amsterdam

Chemistry / Life Sciences
--------------
* Kamerlin, Lynn - Uppsala University
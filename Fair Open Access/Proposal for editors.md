#### Proposal
We propose that editorial boards of subscription journals ask their current publisher to convert 
their journal to one satisfying the principles of [Fair Open Access](http://fairopenaccess.org). 
If the publisher does not agree to this, the editorial board has the option to give 
notice of resignation and set up with another publisher who does agree. 

#### Notes to avoid common misunderstandings: 
It is likely that moving the journal will require a change of name
and the original journal title will continue for some time with new editors, but then cease publication after a few years.
We are not suggesting any change to the way the journal runs, except for becoming open access, using another publisher, and having a different funding model.

#### Recent cases
The entire process has recently been carried out successfully by the board of [J. Algebraic Combinatorics](http://algebraic-combinatorics.org).

#### Futher details

* [Why Fair Open Access?](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Why%20convert%20to%20Fair%20Open%20Access.md)
* [Common questions and concerns, and answers to them](https://gitlab.com/publishing-reform/discussion/tree/master/Fair%20Open%20Access/answers_to_concerns)
* [Recommended publishers](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Details%20of%20publishing%20service%20providers.md)

The members of the executive board of MathOA are happy to discuss any questions or concerns in as much detail as you want.


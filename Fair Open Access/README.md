# [Fair Open Access (FOA)](https://www.fairopenaccess.org/)

#### [FOA principles and list of renowned supporters](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/List%20of%20supporters%20of%20Fair%20Open%20Access.md)

## External links
* Peter Suber has written **the** [book on open access](https://www.dropbox.com/s/5cxsyzs58a5yx5q/9286.pdf?dl=0)
* Fair Open Access Principles: https://www.fairopenaccess.org/
* Free Journal Network (collection of journals satisfying Fair Open Access Principles): http://freejournals.org
* Organizations dedicated to converting existing journals to Fair Open Access: http://mathoa.org, http://lingoa.eu, http://psyoa.org, see https://www.fairopenaccess.org for further details
* The University of Lorraine becomes the [first French institution to join the Fair Open Access Alliance](https://bernardrentier.wordpress.com/2018/04/21/a-fantastic-move-towards-open-science-by-the-university-of-lorraine-france/)

## Important historical information
* [How the scholarly publishing system got into the mess we see now](https://www.theguardian.com/science/2017/jun/27/profitable-business-scientific-publishing-bad-for-science)
* [Comprehensive explanation of open access developments](https://arstechnica.com/science/2016/06/what-is-open-access-free-sharing-of-all-human-knowledge/)

## Documents in this directory
* [Justification for converting a journal to Fair Open Access Model](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Why%20convert%20to%20Fair%20Open%20Access.md)
* [Dealing with concerns about conversion that are commonly expressed by editors](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/README.md)
* [Details of publishing service providers](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Details%20of%20publishing%20service%20providers.md)
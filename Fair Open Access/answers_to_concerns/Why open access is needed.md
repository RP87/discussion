Even when some version of a published article is freely available online (e.g. on arXiv.org),
many things can still go wrong:

* the arXiv version isn't up-to-date; authors either forget, do not feel sufficiently incentivised to do it;
* the preprint has a low google rank (arXiv tends to be ranked well, but search is often hampered by the lack of canonical encoding for mathematical symbols in the title)
* the authors weren't convinced that they were allowed to post on the arXiv (rules can be checked at 
[SHERPA/ROMEO](http://www.sherpa.ac.uk/romeo/index.php) but not all authors know about this)
* the publisher didn't allow posting the final version (e.g. Oxford University Press) 
* the authors have some difficulty with the arXiv (sometimes justified, sometimes not; some authors have hit missing LaTeX packages on the arXiv, stopped trying and never come back).

The problem isn't so much "no one reads a paper not published in an OA journal" 
as it is "the same paper would be read more if published in a (comparable quality) OA journal".
Even if sufficiently interested researcher is sometimes able to find the article freely available on the internet, 
in some version and after spending some time, 
and that a sufficiently internet-competent author will know 
to post their preprint somewhere easily findable and 
update it with the final submitted version, 
this does not mean that authors and readers aren't losing 
when compared to the situation with no paywall at all.

There have been studies on the use of arXiv by mathematicians (and others). 
The results are somewhat surprising. 
Kristine Fowler [found](http://www.istl.org/11-fall/refereed4.html) - see Table 10 - 
that well under half of mathematical researchers post to arXiv systematically. Lariviere et al 
[found](http://onlinelibrary.wiley.com/doi/10.1002/asi.23044/full) 
that usage varies considerably by subfield 
(interestingly, the [arXiv version](https://arxiv.org/abs/1306.3261) 
of this paper is not found by Google Scholar (nor is it updated to the final submitted version).

Versions of papers found on authors' homepages and other places suffer from similar difficulties. 
This [anecdotal experiment](https://gitlab.com/publishing-reform/discussion/issues/17#note_61701750)  is instructive. 


Not knowing whether a version is the version of record makes accurate citation impossible, 
even if the paper can be found. In many cases authors cannot even legally read their own published papers. 
It is a reasonable hypothesis that 
[lack of access hurts junior researchers](https://gitlab.com/publishing-reform/discussion/issues/17) in particular.

Open access is important for other reasons, because it is not just about reading, but about reuse. For example, if a Wikipedia article wants to use material from a published paper, 
it can only do so if the appropriate licence (e.g. CC-BY) is associated to the paper.

More discussion of open access, and openness in general, can be found here: https://poynder.blogspot.com/2018/05/six-questions-about-openness-in-science.html .
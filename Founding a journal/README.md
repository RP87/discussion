# Founding a journal

This directory contains resources for those wanting to start a new journal complying with [Fair Open Access Principles](https://www.fairopenaccess.org).

1. [Justify the journal](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Justification.md)
    * [List of areas that are in need of fair OA journals](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/areas_in_need_of_major_foa_journal.md) *(please help filling it)*
1. [Form a team, involve scholarly community](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Responsiveness%20to%20scholarly%20community.md)
1. [Obtain funding](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Funding.md)
1. Decide on [journal governance](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Governance.md)
1. Decide on [journal policies](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Policies.md)
   * [Open access licences](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Licences.md)
1. [Set up](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Technical%20matters.md) the journal
   * [Service providers](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Details%20of%20publishing%20service%20providers.md)



## External resources

- Starting an Open Access Journal: a step-by-step guide by Martin Eve: [Part 1](https://eve.gd/2012/07/10/starting-an-open-access-journal-a-step-by-step-guide-part-1/), [Part 2](https://eve.gd/2012/07/11/starting-an-open-access-journal-a-step-by-step-guide-part-2/), [Part 3](https://eve.gd/2012/07/11/starting-an-open-access-journal-a-step-by-step-guide-part-2/), [Part 4](https://eve.gd/2012/07/13/starting-an-open-access-journal-a-step-by-step-guide-part-4/), [Part 5](https://eve.gd/2012/07/13/starting-an-open-access-journal-a-step-by-step-guide-part-5/).
# Setting up the journal

*to be written, help if you can*

### Self-hosting vs service providers

Decide between self-hosting, hosting with help of university library services, or [service providers](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Details%20of%20publishing%20service%20providers.md). 
*Pro and contra?* Decide whether to host papers on the journal site or to be an overlay journal associated with a preprint server.

### Other stuff
* Trademarks, domains, ISSN, archiving, DOIs, CLOCKSS, DOAJ, Scopus ...

A service provider or a library service might help with those.
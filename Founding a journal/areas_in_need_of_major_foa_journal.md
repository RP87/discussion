## Mathematics

List of areas in mathematics that are in need of a major [FOA](http://fairopenaccess.org/) journal.
* arXiv & area: area of maths based on [arXiv categories](https://arxiv.org/archive/math).
* FOA journals: column for (some) FOA journals that already exist in that area
* non-FOA to flip: non-FOA journal for which a FOA alternative is needed
* need: wish by X to have a FOA journal in that field
Please fill with wishes where you see a need, add FOA journals, and refine if necessary. 
See also the [discussion on this](https://gitlab.com/publishing-reform/discussion/issues/15) - and the [related discussion](https://gitlab.com/publishing-reform/discussion/issues/7).

|arXiv| area                      | FOA    | diamond | non-free (to flip) |need|
|----|----------------------------|--------|---------|--------------------------------------|----|
| AG | Algebraic Geometry         | [AG](https://algebraicgeometry.nl/#), [Epijournal de Géométrie Algebrique](https://epiga.episciences.org/) | [JSingularities](http://www.journalofsing.org/) | | |
| AT | Algebraic Topology         | | | | |
| AP | Analysis of PDEs           | | | | |
| CT | Category Theory            | [Compositionality](http://www.compositionality-journal.org/), [Cahiers TGDC](http://cahierstgdc.com/), [HS](https://journals.mq.edu.au/index.php/higher_structures/index) | [TAC](http://www.tac.mta.ca/tac/) | | |
| CO | Combinatorics              | [AIC](https://advances-in-combinatorics.scholasticahq.com/), [EJC](http://www.combinatorics.org/), [SLdC](http://www.mat.univie.ac.at/~slc/), [Australasian JC](https://ajc.maths.uq.edu.au/), [AMC](https://amc-journal.eu/index.php/amc)| [TOC](http://toc.ui.ac.ir/) | JCTA, JCTB |@darijgrinberg|
| -> | Algebraic Combinatorics    | [AC](https://alco.centre-mersenne.org/) | | | |
| -> | Analytic Combinatorics    | [Discrete Analysis](http://discreteanalysisjournal.com/), [AADM](http://pefmath.etf.rs/)  |[Online Journal of Analytic Combinatorics](http://web.math.rochester.edu/misc/ojac/) | | |
| CA | Classical Analysis and ODEs| [EJDE](https://ejde.math.txstate.edu/), [EJGTDE](http://www.math.u-szeged.hu/ejqtde/), [JLA](http://logicandanalysis.org/index.php/jla/index) | | | |
| AC | Commutative Algebra        | | | | |
| CV | Complex Variables          | | | | |
| DG | Differential Geometry      | [SIGMA](https://www.emis.de/journals/SIGMA/) | [JSingularities](http://www.journalofsing.org/) | | |
| DS | Dynamical Systems          | [SIGMA](https://www.emis.de/journals/SIGMA/), [EJGTDE](http://www.math.u-szeged.hu/ejqtde/) | | | |
| FA | Functional Analysis        | | | JFA, GAFA, Analysis & PDE |@benoit.kloeckner|
| GM | General Mathematics        | [Documenta Mathematica](https://www.math.uni-bielefeld.de/documenta/Welcome-eng.html), [JEP](https://jep.math.cnrs.fr/index.php/JEP/), [Le Matematiche](https://lematematiche.dmi.unict.it/index.php/lematematiche), [NWEJM](http://math.univ-lille1.fr/~nwejm/), [AHL](https://annales.lebesgue.fr/index.php/AHL/), [AM](https://www.emis.de/journals/AM/) | [Acta Mathematica](https://www.intlpress.com/site/pub/pages/journals/items/acta/_home/_main/index.html), [AIF](http://aif.cedram.org/?lang=en), , [AMBP](http://ambp.cedram.org/?lang=en), [Arkiv for Mathematik](https://intlpress.com/site/pub/pages/journals/items/arkiv/_home/_main/index.html), [NYJM](http://nyjm.albany.edu/), [Revista Colombiana de Matemáticas](http://scm.org.co/revista-colombiana-de-matematicas-en/), [NZJM](http://nzjm.math.auckland.ac.nz/index.php/New_Zealand_Journal_of_Mathematics)  |Adv. Math. |  |
| GN | General Topology           | | | | |
| GT | Geometric Topology         | | | | |
| GR | Group Theory               | | [AGTA](http://www.advgrouptheory.com/journal/index.php#home) | | |
| HO | History and Overview       | | | | |
| IT | Information Theory         | | | | |
| KT | K-Theory and Homology      | | | | |
| LO | Logic                      | [JLA](http://logicandanalysis.org/index.php/jla/index) | | | |
| MP | Mathematical Physics       | | | | |
| MG | Metric Geometry            | | | | |
| NT | Number Theory              | [Integers](http://math.colgate.edu/~integers/), [J. Théor. Nr. Bordx](http://jtnb.cedram.org/?lang=en) | [HRJ](https://hrj.episciences.org/) | | |
| NA | Numerical Analysis         | [SMAI-JCM](https://smai-jcm.math.cnrs.fr/index.php/SMAI-JCM/) | [ETNA](http://etna.math.kent.edu/) | | |
| OA | Operator Algebras          | | | | |
| OC | Optimization and Control   | | |MPA, DO, DAM | @mposs|
| PR | Probability                | [EJP](https://www.imstat.org/journals-and-publications/electronic-journal-of-probability/), [ECP](https://www.imstat.org/journals-and-publications/electronic-communications-in-probability/), [Probability Surveys](http://www.i-journals.org/ps/)| [TSP](http://tsp.imath.kiev.ua/index.html) | PTRF |  |
| QA | Quantum Algebra            | | | | |
| RA | Rings and Algebras         | | | | |
| -> | Non-commutative algebras   | | | J. of Alg. |@darijgrinberg|
| SP | Spectral Theory            | | | | |
| ST | Statistics Theory          | [EJS](http://www.imstat.org/journals-and-publications/electronic-journal-of-statistics/), [Statistics Surveys](http://www.imstat.org/journals-and-publications/statistics-surveys/) | | | |
| SG | Symplectic Geometry        | [SIGMA](https://www.emis.de/journals/SIGMA/) | | | |
| O  | Others                     | | [ELA](http://repository.uwyo.edu/ela/), [JIS](https://cs.uwaterloo.ca/journals/JIS/)  | | |
| -> | Undergraduate-level exposition| | |American Mathematical Monthly, Mathematics Magazine, College Mathematics Journal|@darijgrinberg|

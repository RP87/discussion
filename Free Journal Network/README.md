# [Free Journal Network](http://freejournals.org/)

## What is the Free Journal Network?

Our goal is to promote scholarly journals run according to the [Fair Open Access model](https://www.fairopenaccess.org/); 
roughly, journals that:

- are controlled by the scholarly community;
- have no financial barriers to readers and authors.

Such journals have a long history and many of them are of high quality and prestige. We strive to help such journals to coordinate their efforts to accelerate the creation of a journal ecosystem that will eventually replace the 
commercially controlled journals. Such efforts are complementary to the work of discipline-based organizations such as 
[LingOA](https://www.lingoa.eu/), [MathOA](http://www.mathoa.org/), [PsyOA](http://psyoa.org/), and the 
overarching [FOAA](https://www.fairopenaccess.org/), that focus primarily on moving commercially controlled  journals to the Fair Open Access model.

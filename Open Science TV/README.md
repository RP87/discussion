Hello!

We would like to introduce you to our new project Open Science TV, an independent scientific media platform.
It will promote concepts of Openness in science and innovation and also serve as ecosystem for initiatives and public engagement. Join us!

More about the project, you can read in our [project résumé](https://gitlab.com/publishing-reform/discussion/-/blob/master/Open%20Science%20TV/OpenScienceTV_project.pdf). Feel free to add your comments about our videos in the dedicated forum topics.

**UPDATE!** Our first video is out! Watch it [here](https://www.youtube.com/watch?v=l7dMmR_Fd2M) and discuss it [here](https://gitlab.com/publishing-reform/discussion/-/issues/136).

Please, subscribe to our social network accounts to be always in touch:
- [Twitter](https://twitter.com/OpenScienceTV)
- [Facebook](https://www.facebook.com/opensciencetv)
- [Youtube](https://www.youtube.com/channel/UC1dT_gCSsu7Qkew3W699VkQ/featured?view_as=public)
- [Instagram](https://www.instagram.com/opensciencetv/)
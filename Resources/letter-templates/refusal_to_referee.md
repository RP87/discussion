* (Philip N Cohen from Twitter - a great one)

https://twitter.com/familyunequal/status/769916301645144066

* (Alex Holcombe)

Dear EDITOR NAME, (from https://docs.google.com/document/d/1gDbQ7PpwIjXSpJkQ9-Oqqm8mDTjhlW884xQaNdFWz14/edit?usp=sharing)

Some scientific publishers, such as Elsevier, have higher profit margins than the companies in almost every other legal industry, thanks to scholars' free labor and university library funds (https://alexholcombe.wordpress.com/2013/01/09/scholarly-publishers-and-their-high-profits/). This is one reason that I pledged eight years ago (http://www.openaccesspledge.com/) to spend most of my time supporting alternatives, such as open access journals and preprint servers.
 
Given the announcement of PlanS, in which many European funders will require their grantees to publish solely in journals transforming to full open access, and the cancellation of Elsevier subscriptions by major institutions such as the University of California, these efforts are now urgent (https://101innovations.wordpress.com/2019/06/15/nine-routes-towards-plan-s-compliance-updated/.
 
Your journal has a venerable history of publishing high-quality research, and almost all the credit for that goes to the editorial board and the reviewers, not the publisher, yet somehow the publisher owns the content. We created PsyOA (PsyOA.org) to help journals and scholarly societies transition to alternative publishing models. If you are interested, let me know.

* (David Roberts version)

Dear [editor],

Thank you for considering me as a reviewer for the Journal of Algebra. 
I am convinced that peer-reviewing of research is fundamental 
for the development of mathematics in general. 
Therefore, I am grateful to journal editors like you for organizing 
and monitoring the peer-review and I would gladly contribute to this process.

On the other hand, your journal belongs to the publisher Elsevier, 
who, as I see it, actively hinders the development of science; 
Elsevier charges high prices for subscriptions to "their" articles 
which for the research institutions means either 
a significant drain of research funding money 
or an exclusion from a part of general knowledge.

For this reason, I have signed the "Cost of Knowledge" [1] statement 
and for this reason, I decline your invitation to provide a review.

However, I am personally committed to contribute to any effort that truly supports Open Access. 
If you as the editors are working towards a transition of your journal to real Open Access, 
which among others would mean the principles of FairOA [2], 
I will be happy to support the new journal, including submitting articles and refereeing. 
Also, I understand that such a transition is not simply done together with the current commercial publisher; 
cf. Lingua/Glossa [3] and Journal of Algebraic Combinatorics/Algebraic Combinatorics [4].

Best regards,

* (Mike Taylor version):

Dear EDITOR NAME,

I’m writing to apologise for turning down your request that I review NAME OF PAPER. 
The reason is that I am wholly committed to the free availability 
of all scholarly research to everyone, 
and I cannot in good conscience give my time and expertise 
to a paper that is destined to end up behind PUBLISHER‘s paywall.

I know this can sound very self-righteous — I am sorry if it appears that way. 
I also recognise that there is serious collateral damage 
from limiting my reviewing efforts to open-access journals. 
My judgement is that, in the long term, 
that regrettable damage is a price worth paying, 
and I laid out my reasons a few years ago in this blog post: 
https://svpow.com/2011/10/17/collateral-damage-of-the-non-open-reviewing-boycott/

I hope you will understand my reasons for pushing hard 
towards an open-access future for all our scholarship; 
and I even hope that you might reconsider the time 
you yourself dedicate to PUBLISHER‘s journal, 
and wonder whether it might be more fruitfully spent in helping 
an open-access palaeontology journal to improve its profile and reputation.

Yours, with best wishes,



* (author unknown) https://docs.google.com/document/d/1yKAxZ63XS9xx3v5X-YjwRBcHFU5H9KLYkTN9Pmbikh0/edit

* (Mark C. Wilson version)

I regret that I will no longer be involved with this journal as author or 
reviewer, while it is owned by SpringerNature and run according to its current 
business model. I am sorry to have to make this decision, but I feel I have 
been left no choice. Unless researchers take such steps, serious change will 
not happen in our lifetime.

I strongly urge you and the other editors to contact me to consult on how to 
move the board to another publisher that will act  in the interests of high 
quality science rather than profit. I have experience with this procedure with 
the Journal of Algebraic Combinatorics. 


* (Dmitri Zaitsev version)

Dear [editors]

Many thanks for your referee request that I appreciate.

This puts me in somewhat complicated position, because of my recent involvement with a global group of academics working to help convert established journals to (free to publish) Open Access. The article submitted falls in the borderline area between analysis, PDE and topology, and would be of interest to experts in these areas, who would be less likely, however, to make an effort to retrieve the paper without it being conveniently accessible. Please see this thread for more details on the unfortunate effects of closed access:
https://gitlab.com/publishing-reform/discussion/issues/17

I therefore would like to ask if [journal] would in principle agree to enter a discussion about possible conversion to Open Access. This would by no mean imply any commitment, where the actual conversion would only happen when future viability is established and all stakeholders are fully comfortable. But the mere expression of willingness to explore this option would make my decision much easier, without having to worry about implicitly supporting the system that can disadvantage the authors of the paper and the field it represents.

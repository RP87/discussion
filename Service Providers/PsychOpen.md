# [PsychOpen](http://www.psychopen.eu/)

* European open-access publishing platform for psychology
* Full publishing service
* Free of charge
* Operated by [Leibniz Institute for Psychology Information (ZPID)]([https://www.leibniz-psychology.org/en]), Trier, Germany, as part of ZPID's  overall open science strategy in psychology (free search portal PubPsych, data archive PsychData, repository PsychArchives, etc).

### [Services](https://www.psychopen.eu/for-editors/services/)
* Hosting journal management system (OJS, ARPHA) and technical support
* Editorial and user support
* Publication quality control, including anti-plagiarism checking (iThenticate), automated review of basic statistical results (Statcheck), reference list checking (eXtyles), APA style review
* Copyediting (eXtyles), converting MS Word submissions into high-quality XML (JATS) format, equations are transformed to MathML (LaTeX submission is not supported)
* Page layout and production (Antenna House Formatter)
* DOI registration (Crossref)
* Indexing (e.g., Scopus, PubMed Central, DOAJ)
* Long-term archiving (CLOCKSS)
* Content promotion support

They do not support LaTeX submission. ORCID integration is in preparation (as of 2017) 

### [Mandatory standards](https://www.psychopen.eu/for-editors/publishing-standards/)
* OA with CC BY
* Peer-review
* Endorsement by a scientific organization
* International editorial board
* English metadata and abstracts

### [Example journals](https://www.psychopen.eu/browse-publications/) publishing with PsychOpen
* [Journal of Numerical Cognition](http://jnc.psychopen.eu/)
* [Journal of Social and Political Psychology](http://jspp.psychopen.eu/)
* [Europe's Journal of Psychology](http://ejop.psychopen.eu/)


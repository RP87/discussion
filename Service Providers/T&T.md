# [T&T productions](http://tandtproductions.com/2017/)

* Typesetting and copyediting heavily mathematical articles and books
* 20 years experience

They work or have worked for several mainstream mathematical journals (e.g. Proceedings of the Royal Society, Proceedings of the Royal Society of Edinburgh, Proceedings of the Edinburgh Mathematical Society, Journal de l’Institut Mathématique de Jussieu, SIAM Journal of Optimization) and for prestigious book publishers such as Princeton University Press, SIAM and others. 

They do not publish journals from peer-review to production yet, but have experience in project management (e.g. the Princeton Companion to Mathematics and the Princeton Companion to Applied Mathematics volumes, and they also project managed Proceedings of the Royal Society of Edinburgh for many years) and could extend their services, or they can be used for the production part on top of another service provider. They have also recently developed the ability to produce full-text XML versions of even highly mathematical papers, using MathJax to display equations.

Prices are highly customizable depending on the variety and level of service, with the full service not exceeding a few hundred dollars/euros for a medium-sized paper.
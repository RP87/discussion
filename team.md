# [Public Discussion on Publishing Reform](https://gitlab.com/publishing-reform/discussion/issues) - the team

### Founded and managed by
- [Mark C. Wilson](https://www.cs.auckland.ac.nz/people/mc-wilson) (University of Auckland,  Computer Science and [Free Journal Network](https://freejournals.org/))
- [Dmitri Zaitsev](https://www.maths.tcd.ie/~zaitsev/) (Trinity College Dublin, Mathematics and [Free Journal Network](https://freejournals.org/))

### Technical help
- [Andrew Corcoran](https://cog-phil-lab.org/people/andrew-corcoran/) (Monash University, [Cognition and Philosophy Lab](https://cog-phil-lab.org/), PhD student)
- [Caelen Feller](https://maths.tcd.ie/~fellerc/) (Trinity College Dublin, Mathematics, undergraduate student)
- [Jonathan Klawitter](https://unidirectory.auckland.ac.nz/profile/jkla325) (University of Auckland, Computer Science, PhD student)
- Jasmine Walter (Monash University, PhD student)